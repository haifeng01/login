package com.haifeng.Login.entity.vo;

import com.haifeng.Login.entity.User;


/**
 * 消息模型对象，
 * 状态码：
 * 1 成功
 * 0 失败
 *
 */
public class MassageModel {
    private Integer code = 1;//状态码
    private String msg = "成功！";//提示信息
    private Object object;//回显对象

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "MassageModel{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", object=" + object +
                '}';
    }
}
