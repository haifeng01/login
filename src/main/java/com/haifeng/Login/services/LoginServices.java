package com.haifeng.Login.services;

import com.haifeng.Login.entity.User;
import com.haifeng.Login.entity.vo.MassageModel;
import com.haifeng.Login.mapper.UserMapper;
import com.haifeng.Login.utils.CheckString;
import com.haifeng.Login.utils.GetSqlSession;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class LoginServices {

    /**
     * 1.参数的若空判断
     * 如果参数为空
     * 将状态码、提示信息、回显数据设固到消息模型对泉中,返回消摸型对象
     * 2.调用d层的查询方法,通过用户名查询用户对
     * 3.判断用户对录是否为空
     * 如果为空,将状态码、提示信息、回显数据设到消模型对象中,返回消息摸型对泉
     * 4.判断数据库中查询到的用户密码与前台传递过来的密码作比较
     * 如果不相等,将状态码、提示信感、回显数据设固到消息摸型对泉中,返问消息摸型对泉
     * 5.登录成功,成功状态、提示信息、用户对余设固消感摸型对象,开 eturn
     *
     * @param uname
     * @param upwd
     * @return massageModel
     */

    public static MassageModel userLogin(String uname, String upwd) {
//        创建模型对象
        MassageModel massageModel = new MassageModel();


//        前台发来的数据，用作数据回显
        User u = new User();
        u.setName(uname);
        u.setPassword(upwd);
        massageModel.setObject(u);

//        前台数据非空判断
        if (CheckString.isEmpty(uname) || CheckString.isEmpty(upwd)) {
            System.out.println("用户名或密码为空");
            massageModel.setCode(0);
            massageModel.setMsg("用户名和密码不能为空！");
            System.out.println(massageModel);

            return massageModel;
        }
//        从数据库中查询值
        SqlSession session = GetSqlSession.createSqlSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user = userMapper.queryUserByName(uname);
        System.out.println("查询" + user);
//        用户不存在校验
        if (user == null) {
            massageModel.setCode(0);
            massageModel.setMsg("用户不存在！");
//            System.out.println(massageModel);
            return massageModel;

        }
//        密码一致校验
        if (!upwd.equals(user.getPassword())) {
//            System.out.println("密码不一致");
            massageModel.setCode(0);
            massageModel.setMsg("密码错误！");
//            System.out.println(massageModel);
            return massageModel;
        }
        massageModel.setObject(user);


        return massageModel;
    }
}
