package com.haifeng.Login.mapper;

import com.haifeng.Login.entity.User;

import java.util.List;

/**
 * 用户接口类
 */
public interface UserMapper {
    public User queryUserByName(String username);
    public List<User> findAll();
}
