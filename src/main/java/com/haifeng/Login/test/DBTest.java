package com.haifeng.Login.test;

import com.haifeng.Login.entity.User;
import com.haifeng.Login.mapper.UserMapper;
import com.haifeng.Login.utils.GetSqlSession;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class DBTest {
    @Test
    public void TestDB() {
        SqlSession sqlSession = GetSqlSession.createSqlSession();
        System.out.println(sqlSession);
//不使用接口
        List<User> userList = sqlSession.selectList("userMapper.findAll");
        System.out.println(userList);

//
    }
    //使用接口
    @Test
    public void Test1() {

        /**
         *         SqlSession session = GetSqlSession.createSqlSession();
         *         UserMapper userMapper = session.getMapper(UserMapper.class);
         *         User user = userMapper.queryUserByName(uname);
         */


        SqlSession sqlSession = GetSqlSession.createSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        System.out.println(userMapper);
//        List<User> userList = userMapper.findAll();
//        System.out.println(userList);
//        System.out.println("====================");
        User jim = userMapper.queryUserByName("jim");



        System.out.println(jim);

    }
}
