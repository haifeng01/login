package com.haifeng.Login.controller;

import com.haifeng.Login.entity.vo.MassageModel;
import com.haifeng.Login.services.LoginServices;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/login")
public class loginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uname = request.getParameter("uname");
        String upwd = request.getParameter("upwd");
        MassageModel massageModel=LoginServices.userLogin(uname,upwd );
        if (massageModel.getCode()==1){//成功
//            消息模型对象存入 session域中
            request.getSession().setAttribute("user",massageModel.getObject());
            response.sendRedirect("index.jsp");
        }else {//失败
            request.setAttribute("massageModel",massageModel);
            request.getRequestDispatcher("login.jsp").forward(request,response);
        }
        System.out.println(uname + "-=-=-" + upwd);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);

    }
}
