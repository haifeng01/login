package com.haifeng.Login.controller;

import com.haifeng.Login.entity.User;
import com.haifeng.Login.mapper.UserMapper;
import com.haifeng.Login.utils.GetSqlSession;
import org.apache.ibatis.session.SqlSession;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "TestServlet", value = "/TestServlet")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uname = request.getParameter("uname");
//        String upwd = request.getParameter("upwd");
//        System.out.println("输入"+uname+"=="+upwd);
//        System.out.println("====================");
        SqlSession session = GetSqlSession.createSqlSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user = userMapper.queryUserByName(uname);
        System.out.println("============");
        System.out.println("查询  "+user);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
