package com.haifeng.Login.utils;

public class CheckString {
    /**
     * 判断字符串是否为空
     * @param str
     * @return boolean
     * true为空
     * false非空
     */
    public static boolean isEmpty(String str){
        if (str==null||"".equals(str.trim())){
            return true;
        }else {
            return false;
        }
    }
}
