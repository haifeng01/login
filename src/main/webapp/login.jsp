<%--
  Created by IntelliJ IDEA.
  User: chihaifeng
  Date: 2021/8/29
  Time: 7:25 下午
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户登陆</title>
    <style>
        h1 {
            text-align: center;
        }

        div {
            text-align: center;
        }
    </style>

        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<%--    <script src="js/jquery-3.6.0.js"></script>--%>
    <script>
        /**
         * 给登陆按钮绑定事件
         * 获取用户名密码的值
         * 判断姓名是否为空
         * 若为空span标签赋值提示用户并return
         * 判断密码是否为空
         * 若为空span标签赋值提示用户并return
         * 若都不为空 手动提交表单
         * TODO：密码换成正则表达式
         * TODO：点击事件换成焦点事件
         */

        $(function () {

            $('#login_btn').click(function () {
                var uname = $('#uname').val();
                var upwd = $('#upwd').val();
                console.log(uname);
                console.log(upwd);
                if (isEmpty(uname)) {
                    $('#msg').html('F:用户姓名不可为空');
                    return;
                }

                if (isEmpty(upwd)) {
                    $('#msg').html('F:用户密码不可为空');
                    return;
                }
                $('#loginFrom').submit();

            })

        })

        /**
         * 判断是否为空
         * 空返回true
         * 非空返回false
         * @param str
         * @returns {boolean}
         */
        function isEmpty(str) {
            if (str == null || str.trim() == '') {
                return true;
            } else {
                return false;
            }

        }

    </script>
</head>
<body bgcolor="#7fffd4">
<h1></h1>
<div style="">
    <form action="/login" method="post" id="loginFrom">
        姓名： <input type="text" name="uname" id="uname" value="${massageModel.object.name}"><br>
        密码： <input type="password" name="upwd" id="upwd" value="${massageModel.object.password}"><br>
        <span id="msg" style="font-size: 12px;color: crimson">${massageModel.msg}</span><br>
        <button type="button" id="login_btn">登陆</button>
        <button type="reset" id="reset_btn">重置</button>
    </form>

</div>

</body>
</html>
